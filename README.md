# invidious_db_cleaner

"Cronnable" script that cleans the Invidious database.

Append this to your Cron table :
```
17 2 * * * /path/to/invidious_db_cleaner.sh
```
