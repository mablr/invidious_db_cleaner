psqldb="invidious" # Replace with correct db name

sudo -i -u postgres psql $psqldb -c "DELETE FROM nonces * WHERE expire < current_timestamp;"
sleep 1
sudo -i -u postgres psql $psqldb -c "TRUNCATE TABLE videos;"
sleep 1
sudo -i -u postgres vacuumdb --dbname=$psqldb --analyze --verbose --table 'videos'
sleep 1
sudo -i -u postgres reindexdb --dbname=$psqldb
sleep 3
# Restart postgresql
systemctl start postgresql
systemctl status postgresql --no-pager
sleep 3
# Restart Invidious
sudo systemctl restart invidious
sudo systemctl status invidious --no-pager

echo $(date) "Invidious DB is now clean !" >> invidious_db_cleaner.log
